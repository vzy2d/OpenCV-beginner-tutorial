import cv2
import numpy as np
file_path = r'.\resources\red_panda_snow.mp4'
cap = cv2.VideoCapture(file_path)

while True:
    ret, frame = cap.read()
   
    cv2.imshow("frame", frame)
    
    key = cv2.waitKey(25)
    if key == 27: #ESC
        break

cap.release()
cv2.destroyAllWindows()